# Configurations !

## Annexe des IPs
- R1 = 10.5.40.254
- R2 = 10.5.40.253
- VM = 10.5.30.10

## Les Pings

### Ping de R1 vers R2 et inversement
```
R1#ping 10.5.40.253

Type escape sequence to abort.
Sending 5, 100-byte ICMP Echos to 10.5.40.253, timeout is 2 seconds:
!!!!!
Success rate is 100 percent (5/5), round-trip min/avg/max = 4/21/48 ms
```

```
R2#ping 10.5.40.254

Type escape sequence to abort.
Sending 5, 100-byte ICMP Echos to 10.5.40.254, timeout is 2 seconds:
!!!!!
Success rate is 100 percent (5/5), round-trip min/avg/max = 12/24/40 ms
```

---

### Ping de VM vers R1 et R2
```
[ansible@centos7 incroyable_ansible]$ ping 10.5.40.254
PING 10.5.40.254 (10.5.40.254) 56(84) bytes of data.
64 bytes from 10.5.40.254: icmp_seq=1 ttl=255 time=17.5 ms
64 bytes from 10.5.40.254: icmp_seq=2 ttl=255 time=5.73 ms
64 bytes from 10.5.40.254: icmp_seq=3 ttl=255 time=8.75 ms
64 bytes from 10.5.40.254: icmp_seq=4 ttl=255 time=13.9 ms
^C
--- 10.5.40.254 ping statistics ---
4 packets transmitted, 4 received, 0% packet loss, time 3006ms
rtt min/avg/max/mdev = 5.733/11.504/17.580/4.576 ms
```

```
[ansible@centos7 incroyable_ansible]$ ping 10.5.40.253
PING 10.5.40.253 (10.5.40.253) 56(84) bytes of data.
64 bytes from 10.5.40.253: icmp_seq=1 ttl=254 time=36.3 ms
64 bytes from 10.5.40.253: icmp_seq=2 ttl=254 time=20.1 ms
64 bytes from 10.5.40.253: icmp_seq=3 ttl=254 time=20.6 ms
64 bytes from 10.5.40.253: icmp_seq=4 ttl=254 time=19.3 ms
^C
--- 10.5.40.253 ping statistics ---
4 packets transmitted, 4 received, 0% packet loss, time 3004ms
rtt min/avg/max/mdev = 19.366/24.109/36.311/7.059 ms
```

### Ping de Guest1 vers Guest2
```
guest1> ping 10.5.20.12
84 bytes from 10.5.20.12 icmp_seq=1 ttl=64 time=0.513 ms
84 bytes from 10.5.20.12 icmp_seq=2 ttl=64 time=0.571 ms
84 bytes from 10.5.20.12 icmp_seq=3 ttl=64 time=1.038 ms
84 bytes from 10.5.20.12 icmp_seq=4 ttl=64 time=0.486 ms
```

### Ping de Admin1 vers Admin2
```
admin1> ping 10.5.10.12
84 bytes from 10.5.10.12 icmp_seq=1 ttl=64 time=0.273 ms
84 bytes from 10.5.10.12 icmp_seq=2 ttl=64 time=0.427 ms
84 bytes from 10.5.10.12 icmp_seq=3 ttl=64 time=0.974 ms
84 bytes from 10.5.10.12 icmp_seq=4 ttl=64 time=0.633 ms
84 bytes from 10.5.10.12 icmp_seq=5 ttl=64 time=0.500 ms
```

## Les configurations:

### R1
```
R1#show ip int br
Interface                  IP-Address      OK? Method Status                Protocol
FastEthernet0/0            192.168.122.176 YES DHCP   up                    up      
FastEthernet1/0            unassigned      YES NVRAM  up                    up      
FastEthernet1/0.10         10.5.10.254     YES NVRAM  up                    up      
FastEthernet1/0.20         10.5.20.254     YES NVRAM  up                    up      
FastEthernet1/1            unassigned      YES NVRAM  up                    up      
FastEthernet1/1.30         10.5.30.254     YES NVRAM  up                    up      
FastEthernet2/0            10.5.40.254     YES NVRAM  up                    up 
```

### R2
```
R2#show ip int br
Interface                  IP-Address      OK? Method Status                Protocol
FastEthernet0/0            10.5.40.253     YES NVRAM  up                    up      
FastEthernet1/0            unassigned      YES NVRAM  up                    up      
FastEthernet1/0.10         10.5.10.253     YES NVRAM  up                    up      
FastEthernet1/0.20         10.5.20.253     YES NVRAM  up                    up      
FastEthernet1/1            unassigned      YES NVRAM  administratively down down 
```

### Switch Infra
```
interface Ethernet0/0
 switchport trunk encapsulation dot1q
 switchport mode trunk
 duplex auto
```

### Switch Client 2
```
interface Ethernet0/0
 switchport trunk encapsulation dot1q
 switchport mode trunk
 duplex auto
!
interface Ethernet0/1
 switchport trunk encapsulation dot1q
 switchport mode trunk
 duplex auto
!
interface Ethernet0/2
 switchport trunk encapsulation dot1q
 switchport mode trunk
 duplex auto
!
```